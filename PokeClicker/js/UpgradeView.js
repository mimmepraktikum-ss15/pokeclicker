var App = App || {};
App.UpgradeView = (function () {
    "use strict";
    /* eslint-env browser */
    /*global numeral */
    var upgradeArray = [];

    function initUpgradeList() {
        var upgrade,
            formattedNumber;

        for (var i = 0; i < upgradeArray.length; i++) {
            formattedNumber = numeral(upgradeArray[i].cost).format("0.000 a");

            //global and click upgrades
            if (!upgradeArray[i].lvlUP && upgradeArray[i].bought === false) {
                upgrade = document.createElement("div");
                upgrade.id = "upgrade" + i;
                upgrade.classList.add("upgrade");
                upgrade.style.backgroundImage = "url('images/friendly/" + upgradeArray[i].sprite + "')";

                if (upgradeArray[i].type === -1) {
                    upgrade.title = upgradeArray[i].name + "\n" + "\n" + upgradeArray[i].info + "\n" + "\n" + upgradeArray[i].cost + " Meisterbälle";
                }

                if (upgradeArray[i].type !== -1) {
                    if (upgradeArray[i].cost < 1000000) {
                        upgrade.title = upgradeArray[i].name + "\n" + "\n" + upgradeArray[i].info + "\n" + "\n" + upgradeArray[i].cost + " Dollar";
                    } else {
                        upgrade.title = upgradeArray[i].name + "\n" + "\n" + upgradeArray[i].info + "\n" + "\n" + formattedNumber + " Dollar";
                    }
                }
                document.querySelector("#upgrade").appendChild(upgrade);

                //pokemon evolve upgrades
            } else if (upgradeArray[i].bought === false) {

                upgrade = document.createElement("div");
                upgrade.id = "upgrade" + i;
                upgrade.classList.add("upgrade", "evolveUpgrade");
                upgrade.style.backgroundImage = "url('images/friendly/" + upgradeArray[i].sprite + "')";

                if (upgradeArray[i].cost < 1000000) {
                    upgrade.title = upgradeArray[i].name + "\n" + "\n" + upgradeArray[i].info + "\n" + "\n" + upgradeArray[i].cost + " Dollar";
                } else {
                    upgrade.title = upgradeArray[i].name + "\n" + "\n" + upgradeArray[i].info + "\n" + "\n" + formattedNumber + " Dollar";
                }
                document.querySelector("#evolve" + upgradeArray[i].type).appendChild(upgrade);
            }
        }
    }

    function initWithSavedData() {
        upgradeArray = App.PokeClicker.getCurGameData().upgradeArray;
        initUpgradeList();
    }

    function init() {
        upgradeArray = App.UpgradeData.getUpgrades();
        initUpgradeList();
    }

    return {
        init: init,
        initWithSavedData: initWithSavedData
    };
}());
