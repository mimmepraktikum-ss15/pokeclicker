var App = App || {};
App.PokeClicker = (function () {
    "use strict";
    /* eslint-env browser */
    /*global $, numeral */
    /*eslint-disable no-use-before-define*/
    var pokeDollar = 0,
        masterballs = 0,
        earnedPokeDollar = 0,
        earnedMasterballs = 0,
        pokemonAmount = 0,
        killedEnemys = 0,
        hp = 10,
        hpMax = 10,
        lvl = 1,
        clickDamage = 5,
        clickBoost = 0.1,
        overallDPS = 0,
        pokeArray = [],
        upgradeArray = [],
        masterballsIdArray = [],
        softResetData = {},
        curGameData = {},
        tickIntervall,
        saveIntervall;

    function getCurGameData() {
        curGameData.pokeDollar = pokeDollar;
        curGameData.earnedPokeDollar = earnedPokeDollar;
        curGameData.masterballs = masterballs;
        curGameData.earnedMasterballs = earnedMasterballs;
        curGameData.hpMax = hpMax;
        curGameData.lvl = lvl;
        curGameData.clickDamage = clickDamage;
        curGameData.clickBoost = clickBoost;
        curGameData.overallDPS = overallDPS;
        curGameData.pokeArray = pokeArray;
        curGameData.upgradeArray = upgradeArray;
        curGameData.oldTime = Math.floor((new Date()).getTime() / 1000);

        return curGameData;
    }

    function calculateCurrentDPS() {
        overallDPS = 0;
        for (var i = 0; i < pokeArray.length; i++) {
            overallDPS += pokeArray[i].amount * pokeArray[i].dps;
        }
    }

    function clickAttack() {
        clickDamage = Math.floor(clickBoost * overallDPS);
        if (clickDamage <= 5) {
            clickDamage = 5;
        }
        hp -= clickDamage;
        if (hp <= 0) {
            checkHP();
        } else {
            updateHPView();
        }
    }

    function pokeAttack() {
        for (var i = 0; i < pokeArray.length; i++) {
            hp -= (pokeArray[i].amount * pokeArray[i].dps) / 5;
            checkHP();
        }
    }

    function checkHP() {
        if (hp <= 0) {
            lvl++;
            hp = Math.floor((10 * Math.pow(1.14, lvl)));
            hpMax = hp;

            var rnd = Math.floor(Math.random() * 21);
            document.getElementById("enemy").src = "../PokeClicker/images/enemies/enemy" + rnd + ".gif";

            document.getElementById("healthbar").value = 10;
            pokeDollar += Math.floor(Math.pow(1.13, lvl));
            earnedPokeDollar += Math.floor(Math.pow(1.13, lvl));
            killedEnemys++;
            updateDollarView();
            updateLvl();
        }
    }

    function tick() {
        var health;

        pokeAttack();
        health = (hp / hpMax) * 100;
        document.getElementById("healthbar").value = health;

        updateHPView();
        calculateCurrentDPS();
        updateOverallDPS();
        updateStats();

        //blur and grayscale help to show if a pokemon/upgrade is affordable
        for (var i = 0; i < pokeArray.length; i++) {
            if (pokeArray[i].cost > pokeDollar) {
                $("#pokemon" + i).addClass("grayscale");
            }
            if (pokeArray[i].cost <= pokeDollar) {
                $("#pokemon" + i).removeClass("grayscale");
            }
        }

        for (var k = 0; k < upgradeArray.length; k++) {
            //masterballs upgrades
            if (upgradeArray[k].type === -1) {
                if (upgradeArray[k].cost > masterballs) {
                    $("#upgrade" + k).addClass("grayscale");
                }
                if (upgradeArray[k].cost <= masterballs) {
                    $("#upgrade" + k).removeClass("grayscale");
                }

                //pokedollar upgrades
            } else {
                if (upgradeArray[k].cost > pokeDollar) {
                    $("#upgrade" + k).addClass("grayscale");
                }
                if (upgradeArray[k].cost <= pokeDollar) {
                    $("#upgrade" + k).removeClass("grayscale");
                }
            }

        }
    }

    function updateDollarView() {
        if (pokeDollar < 1000000) {
            $("#pokeDollar").html(pokeDollar);
        } else {
            var formattedNumber = numeral(pokeDollar).format("0.000 a");
            $("#pokeDollar").html(formattedNumber);
        }
    }

    function updateHPView() {
        if (hp < 1000000) {
            $("#hp").html(Math.floor(hp) + " HP");
        } else {
            var formattedNumber = numeral(hp).format("0.000 a");
            $("#hp").html(formattedNumber + " HP");
        }
    }

    function updateOverallDPS() {
        if (overallDPS < 1000000) {
            $("#overallDPS").html(overallDPS + " DPS");
        } else {
            var formattedNumber = numeral(overallDPS).format("0.000 a");
            $("#overallDPS").html(formattedNumber + " DPS");
        }
    }

    function updateMasterballs() {
        $("#masterballs").html(masterballs);
    }

    function updateLvl() {
        $("#lvl").html("Level: " + lvl);
    }


    function updateStats() {
        var formatEarnedPokeDollar,
            formatEarnedMasterballs,
            formatPokemonAmount,
            formatkilledEnemies;

        if (earnedPokeDollar < 1000000) {
            formatEarnedPokeDollar = earnedPokeDollar;
        } else {
            formatEarnedPokeDollar = numeral(earnedPokeDollar).format("0.000 a");
        }
        if (earnedMasterballs < 1000000) {
            formatEarnedMasterballs = earnedMasterballs;
        } else {
            formatEarnedMasterballs = numeral(earnedMasterballs).format("0.000 a");
        }
        if (pokemonAmount < 1000000) {
            formatPokemonAmount = pokemonAmount;
        } else {
            formatPokemonAmount = numeral(pokemonAmount).format("0.000 a");
        }
        if (killedEnemys < 1000000) {
            formatkilledEnemies = killedEnemys;
        } else {
            formatkilledEnemies = numeral(killedEnemys).format("0.000 a");
        }

        pokemonAmount = 0;
        for (var i = 0; i < pokeArray.length; i++) {
            pokemonAmount += pokeArray[i].amount;
        }

        $("#earnedPokeDollar").html(formatEarnedPokeDollar);
        $("#earnedMasterballs").html(formatEarnedMasterballs);
        $("#pokemonAmount").html(formatPokemonAmount);
        $("#killedEnemies").html(formatkilledEnemies);
    }

    function updatePokeAmountView(id) {
        document.getElementById("pokemon" + id).getElementsByClassName("pokeamount")[0].innerHTML = pokeArray[id].amount;
    }

    function updatePokeCostView(id) {
        if (pokeArray[id].cost < 1000000) {
            document.getElementById("pokemon" + id).getElementsByClassName("pokecost")[0].innerHTML = pokeArray[id].cost;
        } else {
            var formattedNumber = numeral(pokeArray[id].cost).format("0.000 a");
            document.getElementById("pokemon" + id).getElementsByClassName("pokecost")[0].innerHTML = formattedNumber;
        }
    }

    function updatePokeDpsView(id) {
        if (pokeArray[id].dps < 1000000) {
            document.getElementById("pokemon" + id).getElementsByClassName("pokedps")[0].innerHTML = pokeArray[id].dps + " DPS";
        } else {
            var formattedNumber = numeral(pokeArray[id].dps).format("0.000 a");
            document.getElementById("pokemon" + id).getElementsByClassName("pokedps")[0].innerHTML = formattedNumber + " DPS";
        }
    }

    function updatePokeNameView(id) {
        document.getElementById("pokemon" + id).getElementsByClassName("pokename")[0].innerHTML = pokeArray[id].name;
    }

    function updatePokeSpriteView(id) {
        document.getElementById("pokemon" + id).getElementsByClassName("pokeimg")[0].style.backgroundImage = "url('images/friendly/" + pokeArray[id].sprite + "')";
    }

    function updateUpgradeView(id) {
        var childNode = document.getElementById("upgrade" + id);
        if (childNode.parentNode) {
            childNode.parentNode.removeChild(childNode);
        }
    }

    function buy(id) {
        if (pokeDollar >= pokeArray[id].cost) {
            pokeDollar -= pokeArray[id].cost;
            pokeArray[id].amount += 1;
            pokeArray[id].cost = Math.round(pokeArray[id].baseCost * Math.pow(1.15, pokeArray[id].amount));

            updateDollarView();
            updatePokeAmountView(id);
            updatePokeCostView(id);
            App.Persistence.saveCurGameData();
        }
    }

    function showStats() {
        var statsWindow = $("#statsWindow");
        var menuWindow = $("#menuWindow");

        if (statsWindow.hasClass("up")) {
            if (!menuWindow.hasClass("up")) {
                menuWindow.slideUp(400);
                menuWindow.toggleClass("up");
            }
            statsWindow.toggleClass("up");
            statsWindow.slideDown(400);
        } else {
            statsWindow.toggleClass("up");
            statsWindow.slideUp(400);
        }
    }

    function showMenu() {
        var menuWindow = $("#menuWindow");
        var statsWindow = $("#statsWindow");

        if (menuWindow.hasClass("up")) {
            if (!statsWindow.hasClass("up")) {
                statsWindow.slideUp(400);
                statsWindow.toggleClass("up");
            }
            menuWindow.toggleClass("up");
            menuWindow.slideDown(400);
        } else {
            menuWindow.toggleClass("up");
            menuWindow.slideUp(400);
        }
    }

    function upgrade(id) {
        var pokeId = upgradeArray[id].type;

        //case: GlobalUpgrade(masterballs)
        if (masterballs >= upgradeArray[id].cost) {
            if (upgradeArray[id].type === -1) {
                masterballs -= upgradeArray[id].cost;
                for (var i = 0; i < pokeArray.length; i++) {
                    pokeArray[i].dps = Math.round(pokeArray[i].dps * upgradeArray[id].dmgBoost);
                    updatePokeDpsView(i);
                }
                upgradeArray[id].bought = true;
                updateUpgradeView(id);
                updateMasterballs();
                masterballsIdArray.push(id);
                App.Persistence.saveCurGameData();
            }
        }

        if (pokeDollar >= upgradeArray[id].cost) {

            //case: ClickerUpgrade
            if (upgradeArray[id].type === -2) {
                pokeDollar -= upgradeArray[id].cost;
                clickBoost = upgradeArray[id].dmgBoost;
                upgradeArray[id].bought = true;
                updateDollarView();
                updateUpgradeView(id);
                App.Persistence.saveCurGameData();
            }

            //case PokemonEvolve
            if (upgradeArray[id].type >= 0) {
                if (upgradeArray[id].lvlUP === true) {
                    if (upgradeArray[id].evoStage === pokeArray[pokeId].evoStage + 1) {
                        pokeDollar -= upgradeArray[id].cost;
                        pokeArray[pokeId].evoStage++;
                        pokeArray[pokeId].dps = Math.round(pokeArray[pokeId].dps * pokeArray[pokeId].evoStage);
                        pokeArray[pokeId].name = upgradeArray[id].evolveName;
                        pokeArray[pokeId].sprite = upgradeArray[id].sprite;
                        upgradeArray[id].bought = true;

                        updateDollarView(pokeId);
                        updatePokeDpsView(pokeId);
                        updatePokeNameView(pokeId);
                        updatePokeSpriteView(pokeId);
                        updateUpgradeView(id);

                        App.Persistence.saveCurGameData();
                    }
                    //case PokemonDmgBoost
                } else {
                    pokeDollar -= upgradeArray[id].cost;
                    pokeArray[pokeId].dps = Math.round(pokeArray[pokeId].dps * upgradeArray[id].dmgBoost);
                    upgradeArray[id].bought = true;

                    updateDollarView(pokeId);
                    updatePokeDpsView(pokeId);
                    updateUpgradeView(id);
                    App.Persistence.saveCurGameData();
                }
            }
        }
    }

    //event handling
    $(document).ready(function () {
        //listern for attack, menu, stats, help and reset
        $("#canvas").click(function () {
            clickAttack();
        });

        $("#stats").click(function () {
            showStats();
        });

        $("#menu").click(function () {
            showMenu();
        });

        $("#softreset").click(function () {
            $("#page").toggleClass("blur");
            softResetConfirm();
        });

        $("#softResetOk").click(function () {
            resetGame();
        });

        $("#softResetCancel").click(function () {
            $("#page").toggleClass("blur");
            $("#softResetView").hide();
            setIntervall();
        });

        $("#hardreset").click(function () {
            $("#page").toggleClass("blur");
            hardResetConfirm();
        });

        $("#hardResetOk").click(function () {
            clearLS();
        });

        $("#hardResetCancel").click(function () {
            $("#page").toggleClass("blur");
            $("#hardResetView").hide();
            setIntervall();
        });

        $("#help").click(function () {
            $("#helpView").fadeIn(1000);
            $("#page").toggleClass("blur");
        });

        $("#helpClose").click(function () {
            $("#helpView").fadeOut(1000);
            $("#page").toggleClass("blur");
        });

        //listener for buying pokemon
        $(".token").click(function () {
            var i = Number(this.id.slice(7));
            buy(i);
        });

        //listener for buying more pokemon at once
        $(".buyMore").click(function () {
            var i = Number(this.id.slice(7)),
                currentAmount,
                wishAmount,
                cumulativePrice10;

            //buy 10
            currentAmount = pokeArray[i].amount;
            wishAmount = currentAmount + 10;
            cumulativePrice10 = (pokeArray[i].baseCost * (Math.pow(1.15, wishAmount) - Math.pow(1.15, currentAmount))) / 0.15;
            if (pokeDollar >= cumulativePrice10) {
                for (var k = 0; k < 10; k++) {
                    buy(i);
                }
            }
        });

        //listener for upgrades
        $(".upgrade").click(function () {
            var i = Number(this.id.slice(7));
            upgrade(i);
        });

        //listener for pokemon hover div
        $(".tokenContainer").on("mouseover", function () {
            var i = Number(this.id.slice(14));
            $("#pokemonHover" + i).show();
        }).on("mouseout", function () {
            var i = Number(this.id.slice(14));
            $("#pokemonHover" + i).hide();
        });
    });

    function initNewGame() {
        //init Pokemon
        App.PokeData.init();
        pokeArray = App.PokeData.getPokemon();
        App.PokeListView.init();

        //init  Upgrades
        App.UpgradeData.init();
        upgradeArray = App.UpgradeData.getUpgrades();
        App.UpgradeView.init();

        //check if masterballs from softReset exists and updates the amount and gobal masterball upgrades
        if (localStorage.getItem("softResetData") != null) {
            softResetData = JSON.parse(window.localStorage.softResetData);

            masterballs = softResetData.masterballs;
            earnedMasterballs = softResetData.earnedMasterballs;
            earnedPokeDollar = softResetData.earnedPokeDollar;


            masterballsIdArray = softResetData.masterballsIdArray;

            for (var k = 0; k < softResetData.masterballsIdArray.length; k++) {
                var id = softResetData.masterballsIdArray[k];

                for (var i = 0; i < pokeArray.length; i++) {
                    pokeArray[i].dps = Math.round(pokeArray[i].dps * upgradeArray[id].dmgBoost);
                    updatePokeDpsView(i);
                }
                upgradeArray[id].bought = true;
                updateUpgradeView(id);
            }
            updateMasterballs();
            updateDollarView();
            updateStats();
        }
    }

    function loadSavedGameData() {
        var savedGameData = App.Persistence.getSavedGameData();
        pokeDollar = savedGameData.pokeDollar;
        earnedPokeDollar = savedGameData.earnedPokeDollar;
        masterballs = savedGameData.masterballs;
        earnedMasterballs = savedGameData.earnedMasterballs;

        hpMax = savedGameData.hpMax;
        hp = savedGameData.hpMax;
        lvl = savedGameData.lvl;
        clickDamage = savedGameData.clickDamage;
        clickBoost = savedGameData.clickBoost;
        pokeArray = savedGameData.pokeArray;
        upgradeArray = savedGameData.upgradeArray;

        App.PokeListView.initWithSavedData();
        App.UpgradeView.initWithSavedData();
        updateDollarView();
        updateMasterballs();
        updateStats();
        updateLvl();
    }

    function softResetConfirm() {
        $("#softResetInfo").html("Soll das Spiel wirklich zurückgesetzt werden? Du würdest " + Math.floor(lvl / 25) + " Meisterbälle erhalten.");
        $("#softResetView").show();
        clearInterval(tickIntervall);
        clearInterval(saveIntervall);
    }

    function hardResetConfirm() {
        $("#hardResetInfo").html("Soll das Spiel wirklich komplett zurückgesetzt werden?");
        $("#hardResetView").show();
        clearInterval(tickIntervall);
        clearInterval(saveIntervall);
    }

    //soft reset
    function resetGame() {
        masterballs += Math.floor(lvl / 25);
        earnedMasterballs += Math.floor(lvl / 25);
        softResetData.earnedPokeDollar = earnedPokeDollar;
        softResetData.earnedMasterballs = earnedMasterballs;
        softResetData.masterballs = masterballs;
        softResetData.masterballsIdArray = masterballsIdArray;

        window.localStorage.removeItem("savedGameData");
        window.localStorage.softResetData = JSON.stringify(softResetData);
        location.reload();
    }

    //hard reset
    function clearLS() {
        window.localStorage.removeItem("savedGameData");
        window.localStorage.removeItem("softResetData");
        location.reload();
    }

    function checkForSavedGame() {
        if (localStorage.getItem("savedGameData") === null) {
            initNewGame();
        } else {
            loadSavedGameData();
        }
    }

    function setIntervall() {
        tickIntervall = window.setInterval(function () {
            tick();
        }, 200);
        saveIntervall = window.setInterval(function () {
            App.Persistence.saveCurGameData();
        }, 5000);
    }

    function init() {
        checkForSavedGame();
        setIntervall();
    }

    return {
        init: init,
        getCurGameData: getCurGameData
    };

}());
