var App = App || {};
App.PokeListView = (function () {
    "use strict";
    /* eslint-env browser */
    /*global numeral */
    var pokeArray = [];


    function initPokeListView() {
        var token,
            image,
            stats,
            name,
            dps,
            costsContainer,
            costs,
            pokeDollarIcon,
            amount,
            tokenPlus,
            tokenContainer,
            buyMore,
            evolve,
            evolveText,
            listBuffer,
            formattedNumber;

        for (var i = 0; i < pokeArray.length; i++) {

            tokenContainer = document.createElement("div");
            tokenContainer.id = "tokenContainer" + i;
            tokenContainer.classList.add("tokenContainer");
            document.querySelector("#right").appendChild(tokenContainer);

            token = document.createElement("div");
            token.id = "pokemon" + i;
            token.classList.add("token", "disable-select");
            document.querySelector("#tokenContainer" + i).appendChild(token);

            image = document.createElement("div");
            image.classList.add("pokeimg");
            image.style.backgroundImage = "url('images/friendly/" + pokeArray[i].sprite + "')";
            document.querySelector("#pokemon" + i).appendChild(image);

            stats = document.createElement("div");
            stats.classList.add("pokestats");
            stats.id = "stats" + i;
            document.querySelector("#pokemon" + i).appendChild(stats);

            name = document.createElement("div");
            name.classList.add("pokename");
            name.innerHTML = pokeArray[i].name;
            document.querySelector("#stats" + i).appendChild(name);

            costsContainer = document.createElement("div");
            costsContainer.id = "costsContainer" + i;
            costsContainer.classList.add("costsContainer");
            document.querySelector("#stats" + i).appendChild(costsContainer);

            costs = document.createElement("div");
            costs.classList.add("pokecost");
            if (pokeArray[i].cost < 1000000) {
                costs.innerHTML = pokeArray[i].cost;
            } else {
                formattedNumber = numeral(pokeArray[i].cost).format("0.000 a");
                costs.innerHTML = formattedNumber;
            }
            document.querySelector("#costsContainer" + i).appendChild(costs);

            pokeDollarIcon = document.createElement("img");
            pokeDollarIcon.classList.add("pokemonCostIcon");
            pokeDollarIcon.src = "../PokeClicker/images/pokedollar2.png";
            document.querySelector("#costsContainer" + i).appendChild(pokeDollarIcon);

            dps = document.createElement("div");
            dps.classList.add("pokedps");
            if (pokeArray[i].dps < 1000000) {
                dps.innerHTML = pokeArray[i].dps + " DPS";
            } else {
                formattedNumber = numeral(pokeArray[i].dps).format("0.000 a");
                dps.innerHTML = formattedNumber + " DPS";
            }
            document.querySelector("#stats" + i).appendChild(dps);

            amount = document.createElement("div");
            amount.classList.add("pokeamount");
            amount.innerHTML = pokeArray[i].amount;
            document.querySelector("#pokemon" + i).appendChild(amount);

            tokenPlus = document.createElement("div");
            tokenPlus.id = "pokemonHover" + i;
            tokenPlus.classList.add("tokenPlus", "disable-select");
            document.querySelector("#tokenContainer" + i).appendChild(tokenPlus);

            buyMore = document.createElement("div");
            buyMore.id = "buyMore" + i;
            buyMore.innerHTML = "buy 10";
            buyMore.classList.add("buyMore", "disable-select");
            document.querySelector("#pokemonHover" + i).appendChild(buyMore);

            evolve = document.createElement("div");
            evolve.id = "evolve" + i;
            evolve.classList.add("evolve", "disable-select");
            document.querySelector("#pokemonHover" + i).appendChild(evolve);

            evolveText = document.createElement("div");
            evolveText.id = "evolveText" + i;
            evolveText.innerHTML = "Evolve: ";
            evolveText.classList.add("evolveText", "disable-select");
            document.querySelector("#evolve" + i).appendChild(evolveText);
        }

        //buffer at the end to fix the last div that could not be shown otherwise
        listBuffer = document.createElement("div");
        listBuffer.id = "listBuffer";
        listBuffer.classList.add("listBuffer", "disable-select");
        document.querySelector("#right").appendChild(listBuffer);
    }

    function initWithSavedData() {
        pokeArray = App.PokeClicker.getCurGameData().pokeArray;
        initPokeListView();
    }

    function init() {
        pokeArray = App.PokeData.getPokemon();
        initPokeListView();
    }

    return {
        init: init,
        initWithSavedData: initWithSavedData
    };
}());
