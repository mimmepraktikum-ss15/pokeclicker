var App = App || {};
App.UpgradeData = (function () {
    "use strict";
    /* eslint-env browser */

    /*
    Info:
    ========================================================================
    type:
    -2  = ClickUpgrade
    -1  = GlobalUpgrade
    >=0 = PokemonUpgrade
    ========================================================================
    lvl Up:
    false   = no evolve
    true    = evolve
    ========================================================================
    */
    var upgradeArray = [];

    function Upgrade() {
        this.name = "Name";
        this.type = 0;
        this.cost = 0;
        this.lvlUP = false;
        this.evolveName = "";
        this.dmgBoost = 0;
        this.info = "";
        this.sprite = "";
        this.bought = false;
    }

    function loadUpgrade(name, type, cost, lvlUP, evolveName, evoStage, dmgBoost, info, sprite, bought) {
        var cur = upgradeArray.length;

        upgradeArray[cur] = new Upgrade();
        upgradeArray[cur].name = name;
        upgradeArray[cur].type = type;
        upgradeArray[cur].cost = cost;
        upgradeArray[cur].evolveName = evolveName;
        upgradeArray[cur].lvlUP = lvlUP;
        upgradeArray[cur].evoStage = evoStage;
        upgradeArray[cur].dmgBoost = dmgBoost;
        upgradeArray[cur].info = info;
        upgradeArray[cur].sprite = sprite;
        upgradeArray[cur].bought = bought;
    }

    function initUpgrades() {
        //click upgrades
        loadUpgrade("Click Upgrade", -2, 10, false, "", 0, 0.15, "+ 5% mehr Schaden durch Klicken", "upgrade_click_2.png", false);
        loadUpgrade("Click Upgrade", -2, 1000000, false, "", 0, 0.20, "+ 10% mehr Schaden durch Klicken", "upgrade_click_2.png", false);
        loadUpgrade("Click Upgrade", -2, 15000000, false, "", 0, 0.30, "+ 20% mehr Schaden durch Klicken", "upgrade_click_2.png", false);

        //pokemon upgrades evolve
        loadUpgrade("Bisasam Evolve", 0, 500, true, "Bisaknosp", 2, 3, "Bisasam entwickelt sich zu Bisaknosp", "bisaknosp.png", false);
        loadUpgrade("Bisknosp Evolve", 0, 5000, true, "Bisaflor", 3, 7, "Bisaknosp entwickelt sich zu Bisaflor", "bisaflor.png", false);
        loadUpgrade("Glumanda Evolve", 1, 6000, true, "Glutexo", 2, 3, "Glumanda entwickelt sich zu Glutexo", "glutexo.png", false);
        loadUpgrade("Glutexo Evolve", 1, 25000, true, "Glurak", 3, 7, "Glutexo entwickelt sich zu Glurak", "glurak.png", false);
        loadUpgrade("Schiggy Evolve", 2, 35000, true, "Schillok", 2, 3, "Schiggy entwickelt sich zu Schillok", "schillok.png", false);
        loadUpgrade("Schillok Evolve", 2, 150000, true, "Turtok", 3, 7, "Schillok entwickelt sich zu Turtok", "turtok.png", false);
        loadUpgrade("Nidoran Evolve", 3, 120000, true, "Nidorino", 2, 3, "Nidoran entwickelt sich zu Nidorino", "nidorino.png", false);
        loadUpgrade("Nidorino Evolve", 3, 600000, true, "Nidoking", 3, 7, "Nidorino entwickelt sich zu Nidoking", "nidoking.png", false);
        loadUpgrade("Kleinstein Evolve", 4, 1200000, true, "Georok", 2, 3, "Kleinstein entwickelt sich zu Georok", "georok.png", false);
        loadUpgrade("Georok Evolve", 4, 6000000, true, "Geowaz", 3, 7, "Georok entwickelt sich zu Geowaz", "geowaz.png", false);
        loadUpgrade("Nebulak Evolve", 5, 17000000, true, "Apollo", 2, 3, "Nebulak entwickelt sich zu Apollo", "apollo.png", false);
        loadUpgrade("Apollo Evolve", 5, 88000000, true, "Gengar", 3, 7, "Apollo entwickelt sich zu Gengar", "gengar.png", false);
        loadUpgrade("Abra Evolve", 6, 125000000, true, "Kadabra", 2, 3, "Abra entwickelt sich zu Kadabra", "kadabra.png", false);
        loadUpgrade("Kadabra Evolve", 6, 1300000000, true, "Simsala", 3, 7, "Kadabra entwickelt sich zu Simsala", "simsala.png", false);
        loadUpgrade("Dratini Evolve", 7, 6000000000, true, "Dragonir", 2, 3, "Dratini entwickelt sich zu Dragonir", "dragonir.png", false);
        loadUpgrade("Dragonir Evolve", 7, 75000000000, true, "Dragoran", 3, 7, "Dragonir entwickelt sich zu Dragoran", "dragoran.png", false);

        //pokemon upgrades boost
        loadUpgrade("Schadensupgrade für Bisasam", 0, 250, false, "", 0, 1.2, "Bisasam und seine Entwicklungen fügen 20% mehr Schaden zu", "bisasam.png", false);
        loadUpgrade("Schadensupgrade für Bisasam", 0, 2500, false, "", 0, 1.5, "Bisasam und seine Entwicklungen fügen 50% mehr Schaden zu", "bisasam.png", false);
        loadUpgrade("Schadensupgrade für Bisasam", 0, 10000, false, "", 0, 2, "Bisasam und seine Entwicklungen fügen 100% mehr Schaden zu", "bisasam.png", false);

        loadUpgrade("Schadensupgrade für Glumanda", 1, 3000, false, "", 0, 1.2, "Glumanda und seine Entwicklungen fügen 20% mehr Schaden zu", "glumanda.png", false);
        loadUpgrade("Schadensupgrade für Glumanda", 1, 10000, false, "", 0, 1.5, "Glumanda und seine Entwicklungen fügen 50% mehr Schaden zu", "glumanda.png", false);
        loadUpgrade("Schadensupgrade für Glumanda", 1, 40000, false, "", 0, 2, "Glumanda und seine Entwicklungen fügen 100% mehr Schaden zu", "glumanda.png", false);

        loadUpgrade("Schadensupgrade für Schiggy", 2, 40000, false, "", 0, 1.2, "Schiggy und seine Entwicklungen fügen 20% mehr Schaden zu", "schiggy.png", false);
        loadUpgrade("Schadensupgrade für Schiggy", 2, 170000, false, "", 0, 1.5, "Schiggy und seine Entwicklungen fügen 50% mehr Schaden zu", "schiggy.png", false);
        loadUpgrade("Schadensupgrade für Schiggy", 2, 900000, false, "", 0, 2, "Schiggy und seine Entwicklungen fügen 100% mehr Schaden zu", "schiggy.png", false);

        loadUpgrade("Schadensupgrade für Nidoran", 3, 240000, false, "", 0, 1.2, "Nidoran und seine Entwicklungen fügen 20% mehr Schaden zu", "nidoran.png", false);
        loadUpgrade("Schadensupgrade für Nidoran", 3, 2100000, false, "", 0, 1.5, "Nidoran und seine Entwicklungen fügen 50% mehr Schaden zu", "nidoran.png", false);
        loadUpgrade("Schadensupgrade für Nidoran", 3, 8000000, false, "", 0, 2, "Nidoran und seine Entwicklungen fügen 100% mehr Schaden zu", "nidoran.png", false);

        loadUpgrade("Schadensupgrade für Kleinstein", 4, 800000, false, "", 0, 1.2, "Kleinstein und seine Entwicklungen fügen 20% mehr Schaden zu", "kleinstein.png", false);
        loadUpgrade("Schadensupgrade für Kleinstein", 4, 7500000, false, "", 0, 1.5, "Kleinstein und seine Entwicklungen fügen 50% mehr Schaden zu", "kleinstein.png", false);
        loadUpgrade("Schadensupgrade für Kleinstein", 4, 42000000, false, "", 0, 2, "Kleinstein und seine Entwicklungen fügen 100% mehr Schaden zu", "kleinstein.png", false);

        loadUpgrade("Schadensupgrade für Nebulak", 5, 45000000, false, "", 0, 1.2, "Nebulak und seine Entwicklungen fügen 20% mehr Schaden zu", "nebulak.png", false);
        loadUpgrade("Schadensupgrade für Nebulak", 5, 270000000, false, "", 0, 1.5, "Nebulak und seine Entwicklungen fügen 50% mehr Schaden zu", "nebulak.png", false);
        loadUpgrade("Schadensupgrade für Nebulak", 5, 1300000000, false, "", 0, 2, "Nebulak und seine Entwicklungen fügen 100% mehr Schaden zu", "nebulak.png", false);

        loadUpgrade("Schadensupgrade für Abra", 6, 230000000, false, "", 0, 1.2, "Abra und seine Entwicklungen fügen 20% mehr Schaden zu", "abra.png", false);
        loadUpgrade("Schadensupgrade für Abra", 6, 1600000000, false, "", 0, 1.5, "Abra und seine Entwicklungen fügen 50% mehr Schaden zu", "abra.png", false);
        loadUpgrade("Schadensupgrade für Abra", 6, 24500000000, false, "", 0, 2, "Abra und seine Entwicklungen fügen 100% mehr Schaden zu", "abra.png", false);

        loadUpgrade("Schadensupgrade für Dratini", 7, 22500000000, false, "", 0, 1.2, "Dratini und seine Entwicklungen fügen 20% mehr Schaden zu", "dratini.png", false);
        loadUpgrade("Schadensupgrade für Dratini", 7, 144000000000, false, "", 0, 1.5, "Dratini und seine Entwicklungen fügen 50% mehr Schaden zu", "dratini.png", false);
        loadUpgrade("Schadensupgrade für Dratini", 7, 1500000000000, false, "", 0, 2, "Dratini und seine Entwicklungen fügen 100% mehr Schaden zu", "dratini.png", false);

        loadUpgrade("Schadensupgrade für Zapdos", 8, 55000000000, false, "", 0, 1.2, "Zapdos fügt 20% mehr Schaden zu", "zapdos.png", false);
        loadUpgrade("Schadensupgrade für Zapdos", 8, 670000000000, false, "", 0, 1.5, "Zapdos fügt 50% mehr Schaden zu", "zapdos.png", false);
        loadUpgrade("Schadensupgrade für Zapdos", 8, 4000000000000, false, "", 0, 2, "Zapdos fügt 100% mehr Schaden zu", "zapdos.png", false);

        loadUpgrade("Schadensupgrade für Arktos", 9, 700000000000, false, "", 0, 1.2, "Arktos fügt 20% mehr Schaden zu", "arktos.png", false);
        loadUpgrade("Schadensupgrade für Arktos", 9, 3200000000000, false, "", 0, 1.5, "Arktos fügt 50% mehr Schaden zu", "arktos.png", false);
        loadUpgrade("Schadensupgrade für Arktos", 9, 26000000000000, false, "", 0, 2, "Arktos fügt 100% mehr Schaden zu", "arktos.png", false);

        loadUpgrade("Schadensupgrade für Lavados", 10, 15000000000000, false, "", 0, 1.2, "Lavados fügt 20% mehr Schaden zu", "lavados.png", false);
        loadUpgrade("Schadensupgrade für Lavados", 10, 96000000000000, false, "", 0, 1.5, "Lavados fügt 50% mehr Schaden zu", "lavados.png", false);
        loadUpgrade("Schadensupgrade für Lavados", 10, 450000000000000, false, "", 0, 2, "Lavados fügt 100% mehr Schaden zu", "lavados.png", false);

        loadUpgrade("Schadensupgrade für Mewtu", 11, 1000000000000000, false, "", 0, 1.2, "Mewtu fügt 20% mehr Schaden zu", "mewtu.png", false);
        loadUpgrade("Schadensupgrade für Mewtu", 11, 14000000000000000, false, "", 0, 1.5, "Mewtu fügt 50% mehr Schaden zu", "mewtu.png", false);
        loadUpgrade("Schadensupgrade für Mewtu", 11, 82000000000000000, false, "", 0, 2, "Mewtu fügt 100% mehr Schaden zu", "mewtu.png", false);

        //global upgrades
        loadUpgrade("Schadensupgrade für ALLE Pokemon", -1, 5, false, "", 0, 1.2, "Alle Pokemon verursachen 20% mehr Schaden", "upgrade_masterballs_2.png", false);
        loadUpgrade("Schadensupgrade für ALLE Pokemon", -1, 10, false, "", 0, 1.4, "Alle Pokemon verursachen 40% mehr Schaden", "upgrade_masterballs_2.png", false);
        loadUpgrade("Schadensupgrade für ALLE Pokemon", -1, 20, false, "", 0, 1.6, "Alle Pokemon verursachen 60% mehr Schaden", "upgrade_masterballs_2.png", false);
        loadUpgrade("Schadensupgrade für ALLE Pokemon", -1, 50, false, "", 0, 1.8, "Alle Pokemon verursachen 80% mehr Schaden", "upgrade_masterballs_2.png", false);
        loadUpgrade("Schadensupgrade für ALLE Pokemon", -1, 100, false, "", 0, 2, "Alle Pokemon verursachen 100% mehr Schaden", "upgrade_masterballs_2.png", false);
    }

    function getUpgrades() {
        return upgradeArray;
    }

    function init() {
        initUpgrades();
    }

    return {
        init: init,
        getUpgrades: getUpgrades
    };

}());
