var App = App || {};
App.Persistence = (function () {
    "use strict";
    /* eslint-env browser */
    var currentData,
        difference = 0,
        offlineKills = 0,
        offlineGold = 0,
        newData;


    function calculateOfflineGold(oldTime) {
        difference = 0;
        offlineGold = 0;
        offlineKills = 0;
        difference = (Math.floor((new Date()).getTime() / 1000)) - oldTime;
        offlineKills = (difference * newData.overallDPS) / newData.hpMax;
        offlineGold = Math.floor(((Math.floor(offlineKills)) * Math.floor(Math.pow(1.13, newData.lvl))) / 4);
        return offlineGold;
    }

    function getSavedGameData() {
        newData = JSON.parse(window.localStorage.savedGameData);
        newData.pokeDollar = newData.pokeDollar + calculateOfflineGold(newData.oldTime);
        return newData;
    }

    function saveCurGameData() {
        currentData = App.PokeClicker.getCurGameData();
        window.localStorage.savedGameData = JSON.stringify(currentData);
    }

    function init() {

    }

    return {
        init: init,
        getSavedGameData: getSavedGameData,
        saveCurGameData: saveCurGameData
    };
}());
