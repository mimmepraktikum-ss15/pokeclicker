var App = App || {};
App.PokeData = (function () {
    "use strict";
    /* eslint-env browser */
    var pokeArray = [];

    function Pokemon() {
        this.name = "Name";
        this.cost = 0;
        this.amount = 0;
        this.dps = 0;
        this.evoStage = 0;
        this.sprite = "";
    }

    function loadPokemon(name, baseCost, cost, amount, dps, evoStage, sprite) {
        var cur = pokeArray.length;

        pokeArray[cur] = new Pokemon();
        pokeArray[cur].name = name;
        pokeArray[cur].baseCost = baseCost;
        pokeArray[cur].cost = cost;
        pokeArray[cur].amount = amount;
        pokeArray[cur].dps = dps;
        pokeArray[cur].evoStage = evoStage;
        pokeArray[cur].sprite = sprite;
    }

    function initPokemon() {
        loadPokemon("Bisasam", 50, 50, 0, 10, 1, "bisasam.png");
        loadPokemon("Glumanda", 400, 400, 0, 40, 1, "glumanda.png");
        loadPokemon("Schiggy", 2500, 2500, 0, 160, 1, "schiggy.png");
        loadPokemon("Nidoran", 18000, 18000, 0, 440, 1, "nidoran.png");
        loadPokemon("Kleinstein", 140000, 140000, 0, 1500, 1, "kleinstein.png");
        loadPokemon("Nebulak", 1500000, 1500000, 0, 10000, 1, "nebulak.png");
        loadPokemon("Abra", 22000000, 22000000, 0, 100000, 1, "abra.png");
        loadPokemon("Dratini", 400000000, 400000000, 0, 5000000, 1, "dratini.png");
        loadPokemon("Zapdos", 3200000000, 3200000000, 0, 35000000, 1, "zapdos.png");
        loadPokemon("Arktos", 95000000000, 95000000000, 0, 85000000, 1, "arktos.png");
        loadPokemon("Lavados", 1200000000000, 1200000000000, 0, 1300000000, 1, "lavados.png");
        loadPokemon("Mewtu", 200000000000000, 200000000000000, 0, 56000000000, 1, "mewtu.png");
    }

    function getPokemon() {
        return pokeArray;
    }

    function init() {
        initPokemon();
    }

    return {
        init: init,
        getPokemon: getPokemon
    };

}());
