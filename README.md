# README #

PokeClicker ist ein Incremental Game, welches das Spielkonzept "Idle-Gaming" anwendet.
Das Thema der Webanwendung bezieht sich auf die faszinierende Welt der © 2015 Pokémon.
### Informationen zum Repository ###

* Universität Regensburg 
* Medieninformatik
* MEI-M 04.3, Multimedia Engineering SS15
* Version 1.0

### Setup ###

* In die Ordnerstruktur navigieren und pokeClicker.html (in Google Chrome) öffnen.

### Beteiligte: ###
* Oliver Pöppel
* Tobias Ackermann